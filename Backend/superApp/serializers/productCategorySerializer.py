from superApp.models.productCategory import ProductCategory
from rest_framework import serializers

class ProductCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductCategory
        fields = ['id', 'name']