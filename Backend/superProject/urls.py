"""superProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from superApp import views

urlpatterns = [
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('users/', views.AllUsers.as_view()),
    path('user/<int:pk>/', views.UserDetailView.as_view()),
    path('userProduct/<int:pk>/', views.UserProductsDetailView.as_view()),
    path('productCategory/', views.ProductCategoryView.as_view()),
    path('productsCategory/', views.AllProductCategory.as_view()),
    path('productCategory/<int:pk>/', views.ProductCategoryView.as_view()),  
    path('product/', views.ProductView.as_view()),
    path('products/',views.AllProducts.as_view()), 
    path('product/<int:pk>/', views.ProductView.as_view()),
    
]

